package bcas.ap.decorator.pattern;

public class CarDecorator implements Car{
	
	protected Car car;
	
	public CarDecorator(Car c){
		this.car=c;
	}
	
	@Override
	public void build() {
		// TODO Auto-generated method stub
		this.car.build();
	}
	

}
