package bcas.ap.decorator.pattern;

public class SportsCar extends CarDecorator{
	
	public SportsCar(Car c) {
		super(c);
	}

	@Override
	public void build(){
		super.build();
		// TODO Auto-generated constructor stub
		System.out.print(" Adding features of Sports Car.");
	}
	
}
