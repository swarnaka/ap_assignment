package bcas.ap.factory.pattern;

public abstract class BankFactory{
	
	public static Bank createloan(BankType type) {
		
		Bank bank = null;
		
		switch(type) {
		
		case BOC:
			bank =new BOC();
			break;
			
		case NSB:
			bank =new NSB();
			break;
			
					
		default:
			// throw some exception
			break;
				
		}
				
		return bank;
		
			}
	
	}

