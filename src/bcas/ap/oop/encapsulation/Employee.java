package bcas.ap.oop.encapsulation;

public class Employee {
	
	private String name;
	private int age;
	private String address;
	private double salary;
	private int telno;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name=name;
	}
	
	
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age=age;
	}
	
		public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address=address;
	}
	
		
	public double getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary=salary;
	}
	
	public int getTelno() {
		return telno;
	}
	public void setTelno(int telno) {
		this.telno=telno;
	}
	

}
