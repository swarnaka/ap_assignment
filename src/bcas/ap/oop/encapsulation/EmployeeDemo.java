package bcas.ap.oop.encapsulation;

public class EmployeeDemo {
	
	public static void main(String[]args) {
		
		Employee employe= new Employee();
		employe.setName("Denci");
		employe.setAge(23);
		employe.setAddress("Main Road Jaffna");
		employe.setSalary(45000);
		employe.setTelno(778338993);
		System.out.println("Name : " +employe.getName()+ " Age : " + employe.getAge() 
		+ " Address: " + employe.getAddress()+  " Salary :" + employe.getSalary()
		+ " Telno : " + employe.getTelno());
		
		
		
	}

}
