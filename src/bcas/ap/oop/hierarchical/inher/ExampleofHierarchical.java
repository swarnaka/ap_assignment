package bcas.ap.oop.hierarchical.inher;

public class ExampleofHierarchical  {
	
	class Person
	{
	   int id;
	   //creating a common method 
	   void printId(int id)
	    {
	        System.out.println("ID:"+id);
	    }
	   
	}
	//extending person class
	class Student extends Person
	{
	    
	    void iden()
	    {
	        System.out.print("Student's");
	    }
	    
	}
	//extending person class
	class Teacher extends Person
	{
	    void iden()
	    {
	        System.out.print("Teacher's ");
	    }
	}
	//extending person class 
	class Clark extends Person
	{
	    void iden()
	    {
	        System.out.print("Clark's");
	    }
	}
	static // driver class 

	class Hierarchical
	{
	    public static void main(String [] er)
	    {
	        Student s=new Student();
	        s.iden();
	        s.printId(11701200);
	        Clark c=new Clark();
	        c.iden();
	        c.printId(1203);
	        Teacher t= new Teacher();
	        t.iden();
	        t.printId(11020);
	        
	    }
	}
	
	

}
