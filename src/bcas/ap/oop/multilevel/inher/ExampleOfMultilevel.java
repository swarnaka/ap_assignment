package bcas.ap.oop.multilevel.inher;;

public class ExampleOfMultilevel {
	
	class Student
	{
		  void printIden()
	    {
	        System.out.print("Student's ");
	    }
	}
	//extending Student class
	class BTech extends Student
	{
	    void Degree()
	    {
	        System.out.println("Degree is Bachelor of Technology");
	    }
	}
	//extending BTech class
	class CSE extends BTech
	{
	    void Stream()
	    {
	        System.out.println("Stream is Computer Sciencec and Engineering");
	    }
	}
	
	class Multilevel
	{
	    public void main(String[]args)
	    {
	        BTech bt=new BTech();
	        bt.printIden();
	        bt.Degree();
	        CSE cse =new CSE();
	        cse.printIden();
	        cse.Stream();
	     	        
	    }
	}

}
