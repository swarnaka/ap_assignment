package bcas.ap.oop.polymorphism;

public class Child {
	
	public void printMessage() {
		System.out.println("Hi welcome from child");
		
	}
	
	public void printMessage(String msg) {
		System.out.println("Hi welcome + msg");
		
	}

}
