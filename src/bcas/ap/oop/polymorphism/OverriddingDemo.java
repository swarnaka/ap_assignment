package bcas.ap.oop.polymorphism;

public class OverriddingDemo {
	
	public static void main(String[]args) {
		
		Child child=new Child();
		child.printMessage();
		child.printMessage("to BCAS");
		
	}

}
