package bcas.ap.oop.abstraction;

public class Circle extends Shape{
	
	private double radius;

	@Override
	public double area() {
		// TODO Auto-generated method stub
		return 3.141 * radius * radius;
	}

	@Override
	public double circumferences() {
		// TODO Auto-generated method stub
		return 2.0 * 3.141 * radius;
	}

}
