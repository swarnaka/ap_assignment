package bcas.ap.uml.lo2;

public class FuelVechicle extends RentedVechicle{
	
	private double nbKms;
	
	public FuelVechicle(double basefee, double nbKms) {
		super(basefee);
		this.nbKms = nbKms;
	}
	public double getMileageFees() {
		if(nbKms<100) {
			   return 0.2*nbKms;
			
		}
			
		else if(nbKms<=400 || nbKms>=100) {
				return 0.3*nbKms;
		}
			
				return 0.3*400+(nbKms-400)*0.5;
			
		}



}
