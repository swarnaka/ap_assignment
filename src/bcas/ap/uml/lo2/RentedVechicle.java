package bcas.ap.uml.lo2;

public class RentedVechicle {
	
	private double basefee;
	

	public RentedVechicle(double baseFees) {
		super();
		this.basefee = baseFees;
	}

	
	public double getBasefee() {
		return basefee;
	}

	public void setBasefee(double basefee) {
		this.basefee = basefee;
	}
}
