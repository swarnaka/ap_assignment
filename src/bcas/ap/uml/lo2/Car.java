package bcas.ap.uml.lo2;

public class Car extends FuelVechicle {
	
	private int nbSeats;
	
	
	public Car(double basefee, double nbKms) {
		super(basefee, nbKms);
		// TODO Auto-generated constructor stub
	}

	public int getNumofseats() {
		return nbSeats;
	}

	public void setNumofseats(int numofseats) {
		this.nbSeats = numofseats;
	}
	
	@Override
	public double getMileageFees() {
		return super.getMileageFees();
	}
	
	public double getCost() {
		return (nbSeats* getBasefee())+getMileageFees();
		
	}


	
}
