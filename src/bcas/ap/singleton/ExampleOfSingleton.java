package bcas.ap.singleton;

public class ExampleOfSingleton {
	
	private static ExampleOfSingleton single_instance = null;   	    
	public String s;  
	 
	    private ExampleOfSingleton ()  
	    {  
	        s = "Hello I am Singleton ";  
	    }  
	     
	    public static ExampleOfSingleton  getInstance()  
	    {  
	        if (single_instance == null)  
	        	
	        	single_instance = new ExampleOfSingleton ();  
	   
	        return single_instance;  
	    }  


}
