package bcas.ap.singleton;

public class SingletonDemo {
	
	public static void main(String[]args) {
		
		ExampleOfSingleton x = ExampleOfSingleton.getInstance();  
		ExampleOfSingleton y = ExampleOfSingleton.getInstance();  
		
	                
			x.s = (x.s).toUpperCase();            
			System.out.println("Welcome To bcas" + x.s);  
	        System.out.println("Design Patterns" + y.s);  
	        
	        System.out.println("\n");  
	            
	        System.out.println("String from x is" + x.s);  
	        System.out.println("String from y is" + y.s);  
	        
			
	}

}
