package bcas.ap.observer.pattern;

public class ManagementObserver implements Observer {
	
	private Employee emp;
    
    public ManagementObserver (Employee employee){
         
        this.emp = employee;            
        this.emp.addObserver(this); 
    }
 
    @Override
    public void sendMessage() {
         System.out.println( "Management is informed about the new salary"
         		+ " ("+ emp.getSalary()+ ") of Mr. "+emp.getName() );
    }

	   

}


