package bcas.ap.observer.pattern;

public class HumanResourceObserver implements Observer {
	
	 private Employee emp;
     
	    public HumanResourceObserver(Employee employee){
	         
	        this.emp = employee;        
	        this.emp.addObserver(this);     
	    }
	 
	    @Override
	    public void sendMessage() {
	         System.out.println( "Human Resources are informed about the new salary "
	         		+ "("+ emp.getSalary()+ ") of Mr. "+emp.getName() );
	    }
 
}
