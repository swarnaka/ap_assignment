package bcas.ap.observer.pattern;

public class ObserverPattern {
	
	public static void main(String[]args) {
		
		Employee emp = new Employee(1, "Jensan", 35000);
		new HumanResourceObserver(emp);
        new ManagementObserver(emp);
        emp.setSalary(35000);
		
	}

}
