package bcas.ap.observer.pattern;

public interface Observer {

	void sendMessage();

}
