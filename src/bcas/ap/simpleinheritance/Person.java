package bcas.ap.simpleinheritance;

public class Person {
	
	 int age,id;
	 String name;
	 
	 void naming(String name){
		 
	   System.out.println("Name:"+name);
	  
	 }
	 
	class Student extends Person{
	    void ageN(int age)
	    {
	        System.out.println("Age of student is:"+age);
	    }
	}
	class Simple
	{
		
	    public void main(String []args)
	    {
	        Student s= new Student();
	        s.naming("Jesi");
	        s.ageN(24);
	    }
	
	}
}
