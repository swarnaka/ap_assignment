package ap.lo3;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import bcas.ap.lo3.DBProperties;

public class StudentsMarks{
	
	private int marks;
		
	 {
	        int markArray[] = new int[5];
	        int i;
	        float sum=0;
	        	        
	        Scanner scan = new Scanner(System.in);
	 
	        System.out.print("Enter marks for 5 Subjects : ");
	        for(i=0; i<5; i++){
	            markArray[i] = scan.nextInt();
	            sum = sum + markArray[i];
	        }
	            
	 
	        System.out.print("Total Marks = " +sum);
	 
	        
	    }
	 
	 public static Connection getConnection() throws ClassNotFoundException , SQLException {
			Connection connection = null;
		//	Class.forName("com.mysql.jdbc.Driver");
			connection=DriverManager.getConnection(DBProperties.URL.value+ DBProperties.DATABASE.value ,DBProperties.USERNAME.value ,DBProperties.PASSWORD.value);
			return connection;
			
		}
		
		// to insert the table record into the database
			

			public void insert(int Id , String fName, String lName,String grade,String subjName1,String subjName2,String subjName3,String subjName4,String subjName5,int total,double average,int maxMarks) throws SQLException, ClassNotFoundException {
				
				Connection connection =null;
				PreparedStatement statement=null;
				
				connection=getConnection();
				statement=connection.prepareStatement("INSERT INTO student_details(Id,fname,lname,grade,subjname1,subjname2,subjname3,subjname4,subjname5,total,average,maxmarks)VALUES(?,?,?,?,?,?,?,?,?)");
				statement.setInt(1, Id);
				statement.setString(2, fName);
				statement.setString(3, lName);
				statement.setString(4, grade);
				statement.setString(5, subjName1);
				statement.setString(6, subjName2);
				statement.setString(7, subjName3);
				statement.setString(8, subjName4);
				statement.setString(9, subjName5);
				statement.setInt(10, total);
				statement.setDouble(11, average);
				statement.setInt(12, maxMarks);
				
				int rowCount=statement.executeUpdate();
				if(statement != null) {
					statement.close();
				}
				
				if(connection != null) {
					connection.close();
				}
				
				
			}

			
			public static StudentsMarks getInstance() {
				// TODO Auto-generated method stub
				return null;
			}
				
				
			
			}
